package ru.tsc.babeshko.tm.exception.field;

import ru.tsc.babeshko.tm.exception.AbstractException;

public final class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Login is empty...");
    }

}