package ru.tsc.babeshko.tm.exception;

public abstract class AbstractException extends RuntimeException {

    public AbstractException(Throwable cause) {
        super(cause);
    }

    public AbstractException(String message) {
        super(message);
    }

}